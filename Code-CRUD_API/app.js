const express = require("express");
const app = express();
const axios = require("axios");
const project = require(`${__dirname}/project`);

const cors = require('cors');
const productRouter = require('./routes/productRoutes');
const cartRouter = require('./routes/cartRoutes');
  
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    req.header("Access-Control-Allow-Origin", "*");
    req.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    req.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
    
  });
app.use(express.json());
app.use("/products" ,productRouter );
app.use("/carts" ,cartRouter );
module.exports = app;
