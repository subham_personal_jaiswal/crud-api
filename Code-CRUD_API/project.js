

exports.projectInformation = async (req, res, next) => {
  try {
    await apiRoot
      .withProjectKey({ projectKey })
      .get()
      .execute()
      .then((data) => {
        console.log("Project information --->", data);
      })
      .catch((error) => {
        console.log("ERROR --->", error);
      });
  } catch (error) {
    console.log("ERROR --->", error);
  }
  console.log("Got project information");
  next();
};



// let result = await apiRoot.products.get( {
//     url: '"https://api.us-central1.gcp.commercetools.com/practise/products',
//     headers: {
//       Authorization: "Bearer IyGgayAE2CVEN1U0haBruMIwEOWq2x8x",
//     },
//   }).execute()
// console.log(result);