const express = require("express");
const cartController = require("./../controllers/cartController");

const router = express.Router();

router
  .route("/")
  .get(cartController.getCartByCartId)
  .post(cartController.createCart)
  .delete(cartController.deleteCartByCartId);
router.route("/updatecart").post(cartController.updateCartByCartId);

module.exports = router;
