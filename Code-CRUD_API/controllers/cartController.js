const apiRoot = require("./../apiRoot");
const projectKey = "practise";
const axios = require("axios");

exports.createCart = async (req, res, next) => {
  try {
    console.log("createCart Updated");
    let body = req.body;
    const headers = {
      Authorization: `${req.headers.authorization}`,
    };
    console.log(headers);

    // TypeScript SDK - Not Working :( getting access Without Authorization header
    // let result = await apiRoot
    //   .withProjectKey({ projectKey })
    //   .carts()
    //   .post({
    //     headers: {
    //       Authorization: `${req.headers.authorization}`,
    //     },
    //     body: body,
    //   })
    //   .execute();

    // Axios Way - Working
    let result = await axios.post(
      "https://api.us-central1.gcp.commercetools.com/practise/carts",

      {
        ...body,
      },
      {
        headers: headers,
      }
    );
    res.status(200).json({ done: result.data });
  } catch (error) {
    const status = error.message.split(" ");
    const statusCode = status[status.length - 1];
    res.status(statusCode).json({ error: error });
  }
  next();
};

exports.getCartByCartId = async (req, res, next) => {
  try {
    console.log("getCartByCartId");
    const ID = req.body.cartid;
    const headers = {
      Authorization: `${req.headers.authorization}`,
    };

    // TypeScript SDK - Not Working :( getting access Without Authorization header
    // let result = await apiRoot
    //   .withProjectKey({ projectKey })
    //   .carts()
    //   .withId({ ID })
    //   .get({
    //     headers: {
    //       Authorization: `${req.headers.authorization}`,
    //     },
    //   })
    //   .execute();

    // Axios Way - Working 
    let result = await axios.get(
      `https://api.us-central1.gcp.commercetools.com/practise/carts/${ID}`,
      {
        headers: headers,
      }
    );
    console.log(result);
    res.status(200).json({ done: "Done"});
  } catch (error) {
    const status = error.message.split(" ");
    const statusCode = status[status.length - 1];
    res.status(statusCode).json({ error: error });
  }
  next();
};

exports.updateCartByCartId = async (req, res, next) => {
  let validBody = req.body;
  const ID = req.body.cartid;
  console.log(validBody);
  const headers = {
    Authorization: `${req.headers.authorization}`,
  };
  try {
    console.log("updateCartByCartId");
     // TypeScript SDK - Not Working :( getting access Without Authorization header
    // let result = await apiRoot
    //   .withProjectKey({ projectKey })
    //   .carts()
    //   .withId({ ID })
    //   .post({
    //     headers: {
    //       Authorization: `${req.headers.authorization}`,
    //     },
    //     body: validBody,
    //   })
    //   .execute();

    // Axios Way - Working 
      let result = await axios.post(
        `https://api.us-central1.gcp.commercetools.com/practise/carts/${ID}`,
        
      {
        ...validBody,
      },
        {
          headers: headers,
        }
      );
    console.log(result);
    res.status(200).json({ done: "Update Done" });
  } catch (error) {
    console.log(error);
    console.log(error.status);
    res.json({ error: error });
  }
  next();
};

exports.deleteCartByCartId = async (req, res, next) => {
  const ID = req.body.cartid;
  const version = req.body.version * 1;
  const headers = {
    Authorization: `${req.headers.authorization}`,
  };
  try {
    console.log("DeleteCartByCartId");
     // TypeScript SDK - Not Working :( getting access Without Authorization header
    // let result = await apiRoot
    //   .withProjectKey({ projectKey })
    //   .carts()
    //   .withId({ ID })
    //   .delete({
    //     queryArgs: {
    //       version: version,
    //     },
    //     headers: {
    //       Authorization: `${req.headers.authorization}`,
    //     },
    //   })
    //   .execute();

    // Axios Way - Working
      let result = await axios.delete(
        `https://api.us-central1.gcp.commercetools.com/practise/carts/${ID}?version=${version}`,
        {
          headers: headers,
        }
      );

    
    console.log(result);
    res.status(200).json({ done: "Deleted" });
  } catch (error) {
    console.log(error);
    res.json({ error: error });
  }
  next();
};

//Few Doubts
//1.Works Without Authorization  , How ?
//  For Create Cart , get Cart , delete Cart  TypeScript SDK - Not Working :( getting access Without Authorization header
//2.Doubt Search With variant.attributes.color => Filter
